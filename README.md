# BilDoktorn Stockholm AB

A car workshop mobile application for iOS and Android with React Native framework. 

BilDoktorn Stockholm AB is an imaginary company. The goal of this project is to learn how to develop mobile application using React Native targeting iOS and Android.

Application Features:

- Shows all or particular BilDoktorn Stockholm AB car workshops in Stockholm which can be filtered by user
- Select a workshop and book a time for car service or other repairs
- User profile and previous booking history management 





