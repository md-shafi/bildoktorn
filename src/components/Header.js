'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Platform, Image } from 'react-native';

import { BASE_STYLES } from 'app/constants/baseStyles';
import { COLORS } from 'app/constants/colors';

export default class Header extends Component {

    _handlePress() {
        this.props.onPressHeaderLink(this.props.headerLink);
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.containerInner}>
                    <View style={styles.linkWrap}>
                        {this.props.headerBack != null && typeof(this.props.headerBack) != 'undefined' ?
                        <TouchableOpacity onPress={this.props.headerBack}>
                            <Image
                                style={BASE_STYLES.BUTTON.navBar} resizeMode={'contain'}
                                source={require('app/resources/icons/back32.png')}
                            />
                        </TouchableOpacity> : null}
                    </View>

                    <View style={styles.titleWrap}>
                        <Text style={BASE_STYLES.TEXTFIELD.headerTitle}>
                            {this.props.headerTitle}
                        </Text>
                    </View>

                    <View style={styles.spacer}>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flexDirection: 'row',
        height: 50,
        // marginTop: Platform.OS === 'ios' ? 22 : 0,
        backgroundColor: COLORS.appThemeColor,
        // backgroundColor: '#f7f7f7',
        borderBottomWidth: 1,
        borderColor: COLORS.seperatorBorderColor,
    },
    containerInner: {
        flexDirection: 'row',
        flex: 1,
        marginVertical: 5,
        marginHorizontal: 3,
        backgroundColor: COLORS.buttonBackgroundColor,
        borderRadius: 3,        

    },
    spacer: {
        flex: 1,
    },
    titleWrap: {
        flex: 2,
        // marginTop: -10,
        justifyContent: 'center',
        // backgroundColor: 'green',
    },
    linkWrap: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        // backgroundColor: 'orange',
    },
    headerLink: {
        color: 'white',
        paddingLeft: 10, 
    },
});
