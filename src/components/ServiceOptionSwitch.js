'use strict';

import React, { Component } from 'react';
import { Switch, Text, TouchableHighlight, TouchableOpacity, View, StyleSheet, Image} from 'react-native';
import { connect } from 'react-redux';

const CAR_MODEL = ['AUDI', 'SEAT', 'SKODA', 'VOLKSWAGEN', 'VW TRP'];
import { COLORS } from 'app/constants/colors';
import { BASE_STYLES } from 'app/constants/baseStyles';

class CarFilter extends Component {
    constructor(){
        super();
        this.state = {
            audiSelected: true,
            seatSelected: true,
            skodaSelected: true
        };
    }

    render() {
        return (
            <View style={{ flex: 1}}>
                <View style={BASE_STYLES.MODAL.modalRow}>
                    <View>
                        <Text style={{fontSize: 18}}> Byta tarkarblad </Text>
                    </View>
                    <View>
                        <Switch
                            onValueChange={(value) => this.props._filterAudi(value)}
                            onTintColor={'silver'}
                            thumbTintColor={this.props.audiSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
                            tintColor={'silver'}
                            value={this.props.audiSelected} />
                    </View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                    <View>
                        <Text style={{fontSize: 18}}> Glödlampor byte </Text>
                    </View>
                    <View>
                        <Switch
                            onValueChange={(value) => this.props._filterSeat(value)}
                            onTintColor={'silver'}
                            thumbTintColor={this.props.seatSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
                            tintColor={'silver'}
                            value={this.props.seatSelected} />
                    </View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                    <View>
                        <Text style={{fontSize: 18}}> Hyr bil från 399 kr </Text>
                    </View>
                    <View>
                        <Switch
                            onValueChange={(value) => this.props._filterSkoda(value)}
                            onTintColor={'silver'}
                            thumbTintColor={this.props.skodaSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
                            tintColor={'silver'}
                            value={this.props.skodaSelected} />
                    </View>
                </View>
            </View>
        );
  }
}

const mapStateToProps = (state) => {
    console.log('%%% audi' , state.carFilter.audiSelected);
    // console.log('%%% seat' , state.seatSelected);
    // console.log('%%% page:' , state.page);
    return{
        audiSelected: state.carFilter.audiSelected,
        seatSelected: state.carFilter.seatSelected,
        skodaSelected: state.carFilter.skodaSelected,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        _filterAudi: (value) => {
            dispatch({
                type: 'FILTER_AUDI',
                data: value
            });
        },
        _filterSeat: (value) => {
            dispatch({
                type: 'FILTER_SEAT',
                data: value
            });
        },
        _filterSkoda: (value) => {
            dispatch({
                type: 'FILTER_SKODA',
                data: value
            });
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarFilter);

// 'use strict';

// import React, { Component } from 'react';
// import { Switch, Text, TouchableHighlight, TouchableOpacity, View, StyleSheet, Image} from 'react-native';

// // const CAR_MODEL = ['AUDI', 'SEAT', 'SKODA', 'VOLKSWAGEN', 'VW TRP'];
// import { COLORS } from 'app/constants/colors';
// import { BASE_STYLES } from 'app/constants/baseStyles';

// export default class CarFilter extends Component {
// 	constructor(){
// 		super();
// 		this.state = {
// 			audiSelected: false,
// 			seatSelected: false,
// 			skodaSelected: false,
// 			volkswagenSelected: false,
// 			vwTrpSelected: false,
//             audiSwitchDisable: false,
//             seatSwitchDisable: false,
//             skodaSwitchDisable: false,
//             volkswagenSwitchDisable: false,
//             vwTrpSwitchDisable: false,
// 		};
// 	}

// 	render() {
//         return (
//          	<View style={{ flex: 1}}>
//                 <View style={BASE_STYLES.MODAL.modalRow}>
//                 	<View>
//                 		<Text style={{fontSize: 18}}> Byta tarkarblad </Text>
//                 	</View>
//                 	<View>
//                 		<Switch
//     						onValueChange={(value) => 
//                                 this.setState({
//                                     audiSelected: value })}
//     						disabled={this.state.audiSwitchDisable}
//                             onTintColor={COLORS.modalActiveBackground}
//                             thumbTintColor={this.props.audiSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
//                             tintColor={COLORS.iconMuteColor}
//     						value={this.state.audiSelected} />
//                 	</View>
//                 </View>
//                 <View style={BASE_STYLES.MODAL.modalRow}>
//                 	<View>
//                 		<Text style={{fontSize: 18}}> Glödlampor byte </Text>
//                 	</View>
//                 	<View>
//                 		<Switch
//     						onValueChange={(value) => this.setState({seatSelected: value})}
//                             disabled={this.state.seatSwitchDisable}
//                             onTintColor={COLORS.modalActiveBackground}
//                             thumbTintColor={this.props.audiSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
//                             tintColor={COLORS.iconMuteColor}
//     						value={this.state.seatSelected} />
//                 	</View>
//                 </View>
//                 <View style={BASE_STYLES.MODAL.modalRow}>
//                 	<View>
//                 		<Text style={{fontSize: 18}}> Hyr bil från 399 kr </Text>
//                 	</View>
//                 	<View>
//                 		<Switch
//     						onValueChange={(value) => this.setState({skodaSelected: value})}
//                             disabled={this.state.skodaSwitchDisable}
//                              onTintColor={COLORS.modalActiveBackground}
//                             thumbTintColor={this.props.audiSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
//                             tintColor={COLORS.iconMuteColor}
//     						value={this.state.skodaSelected} />
//                 	</View>
//                 </View>
//           	</View>
//         );
//   }
// }

