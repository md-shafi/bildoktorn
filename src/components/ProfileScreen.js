'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';

import Header from './Header';
import { BASE_STYLES } from 'app/constants/baseStyles';

export default class ProfileScreen extends Component {

    static navigationOptions = {
        tabBarLabel: 'Profil',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../resources/icons/profileEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };

    _handleBooking() {
        this.props.showBooking();
    }    

    _handleLogin() {
        this.props.showLogin();
    }

    render(){
        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Profil'}
                    headerBack={() => this.props.navigation.goBack()}
                />
                <ScrollView style={styles.scrollViewContainer}>
                    <Text style={BASE_STYLES.TEXTFIELD.headerText}>
                        Tidigare Bokningar
                    </Text>
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Om du loggar in kan du se tidigare bokningar
                    </Text>
                    <View style={BASE_STYLES.SPACER}>
                        <TouchableOpacity onPress={() =>this._handleLogin()} >
                            <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                                Logga in
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Om du inte har ett konto sedan tidigare så skapas ett när 
                        du gör din första bokning
                    </Text>
                    <View style={BASE_STYLES.SPACER}>
                        <TouchableOpacity onPress={() =>this._handleBooking()} >
                            <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                                Boka tid
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        paddingHorizontal: 10,
    },
    icon: {
        width: 26,
        height: 26,
    },
});