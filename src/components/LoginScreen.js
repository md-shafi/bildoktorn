'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image, ScrollView, KeyboardAvoidingView } from 'react-native';

import Header from './Header';
import { BASE_STYLES } from 'app/constants/baseStyles';

export default class LoginScreen extends Component {
    constructor(){
        super();
        this.state = {
            email:'',
            password:'', 
        }
    }

    static navigationOptions = {
        tabBarLabel: 'Login',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../resources/icons/loginEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };

    _onEmailChange(event){
        let inputValue = event.nativeEvent.text;
        console.log("###### inputValue: ", inputValue);
        this.setState({
            email: inputValue
        });

    } 


    _handleBooking() {
        this.props.showBooking();
    }    

    _handleLogin() {
        this.props.showLogin();
    }

    render(){
        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Logga in'}
                />
                <ScrollView style = {styles.scrollViewContainer}>
                {/*<KeyboardAvoidingView  behavior="padding">*/}
                    <View style={{marginTop: 10}}>
                    </View>
                    <Text style={[BASE_STYLES.TEXTFIELD.bodyText]}>
                        E-post
                    </Text>
                    <TextInput style = {BASE_STYLES.INPUTFILED.basic}
                        placeholder = {'E-post'}
                        returnKeyType ='next'
                        autoCorrect = {false}
                        autoCapitalization = 'none'
                        blurOnSubmit = {false}
                        keyboardType ='email-address'
                        underlineColorAndroid ='rgba(0,0,0,0)'
                        onChange = {(event)=>{this._onEmailChange(event)}}
                        onSubmitEditing = {(event)=>this.refs.passwordInput.focus()} 
                    />
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Lösenord
                    </Text>                
                    <TextInput style={BASE_STYLES.INPUTFILED.basic}
                        ref={'passwordInput'}
                        placeholder={'Lösenord'}
                        secureTextEntry
                        returnKeyType='go'
                        underlineColorAndroid='rgba(0,0,0,0)' 
                    />
                    <View style={BASE_STYLES.SPACER}>
                    </View>
                    <TouchableOpacity 
                        // onPress = {() => this.props.navigation.dispatch({ type: 'ResetPass' })}>
                        onPress = {() => this.props.navigation.navigate('ResetPass')}>
                        <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                            Glömt lösenord
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={BASE_STYLES.BUTTON.bodyWide}>
                        <View style={styles.buttonTextWrap}>
                            <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                                Logga In
                            </Text>
                        </View>                    
                    </TouchableOpacity>
                {/*</KeyboardAvoidingView>*/}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        paddingHorizontal: 10,
    },
    icon: {
        width: 26,
        height: 26,
    },
    inputFieldPlaceHolder : {
        color: '#777777',
    },
    buttonTextWrap: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor: 'green',
    },
});