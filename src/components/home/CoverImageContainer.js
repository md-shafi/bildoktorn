'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity} from 'react-native';

import ImageResize from './ImageResize';

export default class MainImage extends Component {

    render(){
        return(
            <View style={styles.PinContent}>
                <ImageResize
                    source={this.props.source}
                    originalWidth={this.props.originalWidth}
                    originalHeight={this.props.originalHeight}
                />
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
    },
});