'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Platform, Dimensions } from 'react-native';

import CompanyContactInfo from 'app/components/home/CompanyContactInfo';
import CoverImageContainer from './CoverImageContainer';
import { BASE_STYLES } from 'app/constants/baseStyles';

import { NavigationActions } from 'react-navigation';

export default class Home extends Component {
    constructor() {
        super();
        this.homeBanar = {
            source: require('app/resources/images/homeBanar.png'),
            originalWidth: 804,
            originalHeight: 170          
        };       
        this.homeModel = {
            source: require('app/resources/images/coverImageAudi.jpg'),
            originalWidth: 3000,
            originalHeight: 1993          
        };
        this.state = {
            // height: 400,
            // weidth: 600,
            height: Dimensions.get('window').height,
            weidth: Dimensions.get('window').width,


        }
    }

    static navigationOptions = {
        tabBarLabel: 'Hem',
        // title: 'Hem',
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('app/resources/icons/homeEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
          />
        ),
    };

    measureView(event) {
        // let {width, height} = event.nativeEvent.layout;
        this.setState({
            width: event.nativeEvent.layout.width,
            height: event.nativeEvent.layout.height
        });
    }

    renderImage() {
        if(this.state.height < this.state.width) {
            return (
                <View style={{alignItems: 'center'}}>
                    <Image style={{width: this.state.width - 100, height: this.state.height,
                                 }} 
                        source={this.homeBanar.source}
                        resizeMode={'contain'}
                    />
                </View>


            );
        } else {
            return (
                <View>
                    <View style={{alignItems: 'center',}}>
                        <Image style={{width: this.state.width - 50, height: 100
                                     }} 
                            source={this.homeBanar.source}
                            resizeMode={'contain'}
                        />
                    </View>
                    <View style={{paddingLeft: 40, alignItems: 'center'}}>
                        <Image style={{width: this.state.width, height: 300, 
                                     }} 
                            source={this.homeModel.source}
                            resizeMode={'contain'}
                        />
                    </View>
                </View>
            );
        }        
    }

    render() {
        return(
                <View 
                    style={{justifyContent: this.state.height < this.state.width ? 'space-around' : 'space-between'},
                        {paddingTop: this.state.height < this.state.width ? 0 : 100, backgroundColor: 'white', flex: 1 }} 
                    onLayout={(event) => this.measureView(event)} >
                    <View style={styles.topContainer}>
                        {this.renderImage()}
                    </View>
                    <View style={styles.bottomContainer}>
                        {/*<CompanyContactInfo />*/}
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 26,
        height: 26,
    },
});

