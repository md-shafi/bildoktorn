'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text, Linking, TouchableOpacity} from 'react-native';

import { web, phonecall } from 'react-native-communications';
import { BASE_STYLES } from 'app/constants/baseStyles';

export default class CompanyContactInfo extends Component {

    // _handlePress(url) {
    //     Linking.canOpenURL(url).then(supported => {
    //       if (supported) {
    //         Linking.openURL(url);
    //       } else {
    //         console.log('Don\'t know how to open URI: ' + url);
    //       }
    //     }).catch((error)=>{
    //         console.log("Api call error");
    //         alert(error.message);
    //     });
    // }

    render(){
        let url = "https://www.google.se/?gfe_rd=cr&ei=aFJEWfvkAomr8wepuYHQBA#q=bildoktorn+stockholm";
        let phoneNumber = '087080900';
        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={() => { web(url) }}>
                    <View>
                        <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                            {'Till hemsidan'}
                        </Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.bottomTextContainer}>
                    <Text style={{fontSize: 16}}>
                        {'Tel vxl - '}
                    </Text>
                    <TouchableOpacity onPress={() => { phonecall(phoneNumber, true) }}>
                        <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                            {'08-70 80 900'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        marginBottom: 20,
        paddingHorizontal: 20,
    },
    link: {
        color: '#0a60fe',
    },
    bottomTextContainer: {
        paddingTop: 5,
        flexDirection: 'row',
    },

});
