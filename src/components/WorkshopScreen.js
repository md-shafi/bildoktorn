'use strict';

import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const WorkshopScreen = () => (
    <View style={styles.container}>
        <Text style={styles.welcome}>
            Workshop Screen
       </Text>
    </View>
);

WorkshopScreen.navigationOptions = {
    title: 'Workshop',
    tabBarLabel: 'Verkstäder',
    tabBarIcon: ({ tintColor }) => (
    <Image
        source={require('../resources/icons/mapEnable.png')}
        style={[styles.icon, {tintColor: tintColor}]}
    />
    ),
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    icon: {
        width: 26,
        height: 26,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});


export default WorkshopScreen;