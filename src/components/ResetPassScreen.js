'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image, ScrollView } from 'react-native';

import Header from './Header';
import { BASE_STYLES } from 'app/constants/baseStyles';

export default class ResetPassScreen extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            input : '',
            message: '',
            backgroundColor: 'rgba(0,0,0,0)',
            showMessage: false,
        }
    }

    static navigationOptions = {
        tabBarLabel: 'Login',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../resources/icons/loginEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };

    _onChange(event) {
        console.log('###', event.nativeEvent.text);
        var inputValue = event.nativeEvent.text; 
        // var inputValue = this._inputFilter(event.nativeEvent.text); 
        // this.props.output(inputValue);
        this.setState({
            input: inputValue
        });
    }

    _onPress() {
        // let value = this.refs.myTextInput._lastNativeText;
        var inputValue=this.state.input;
        this.setState({
            showMessage: true
        });
        // let response = true;
        let response = inputValue;
        if (!inputValue) {
            this.setState({
                message: 'Skriv e-post adress',
                backgroundColor: '#F58A81'
            });
            // this._blankInput();
        } else if (!this._validateEmail(inputValue)) {
            // console.log('ogiltigt e-post address');
            this.setState({
                message: 'Ogiltigt e-post address',
                backgroundColor: '#F58A81'
            });
            // this._invalidEmail();
        } else if (response!=='sd@sd.sd' ) {
            this.setState({
                message: 'E-post addressen hittades inte i databasen',
                backgroundColor: '#F58A81'
            });
            // console.log('ogiltigt e-post address');
            // this._invalidEmail();
        } else {
            this.setState({
                message: 'Länken för att återställa lösenordet har skickats till den angivna e-postadressen',
                backgroundColor: '#7FB67A'
            });
            // console.log('#',inputValue);
            // this._passwordSent();
        }

    } 

    _validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    _blankInput() {
        console.log('#########', this.state.backgroundColor);
        return {
            backgroundColor: this.state.backgroundColor
        }
    }

    _invalidEmail() {
        return {
            backgroundColor: this.state.backgroundColor
        }
    }

    _passwordSent() {
        return {
            backgroundColor: this.state.backgroundColor
        }
    }

    render(){
        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Återställa lösenordet'}
                    headerBack={() => this.props.navigation.goBack()}
                />
                <View style={styles.bodyContainer}>
                    {this.state.showMessage ? messageContainer : null}
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        E-post
                    </Text>
                    <TextInput 
                        // style={[styles.inputField, this._onPress()]}
                        style={BASE_STYLES.INPUTFILED.basic}
                        // ref={'myTextInput'}
                        placeholder={'E-post'}
                        returnKeyType='next'
                        autoCorrect={false}
                        autoCapitalization='none'
                        blurOnSubmit={false}
                        keyboardType='email-address'
                        underlineColorAndroid='rgba(0,0,0,0)'

                        onChange={(event)=>this._onChange(event)}
                        value={this.state.input}
                        onSubmitEditing={()=>this._onPress()}
                    />
                    <TouchableOpacity style={BASE_STYLES.BUTTON.bodyWide} onPress={()=>this._onPress()} >
                        <View style={styles.buttonTextWrap}>
                            <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                                SKICKA
                            </Text>
                        </View>                    
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    bodyContainer: {
        paddingHorizontal: 10,
    },
    icon: {
        width: 26,
        height: 26,
    },
    inputFieldPlaceHolder : {
        color: '#777777',
    },
    buttonTextWrap: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor: 'green',
    },
});