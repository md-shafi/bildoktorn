'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput,  View, ScrollView, 
    TouchableHighlight, TouchableOpacity, Picker, Image, Dimensions } from 'react-native';
import { web } from 'react-native-communications';

import Header from '../Header';
import { BASE_STYLES } from 'app/constants/baseStyles';
import ModelSelect from 'app/components/pickers/pickerOverlay';

export default class UserContactInfo extends Component {

    constructor(props) {
        super(props);
        this.state={
            showManualInput: false,
            modalVisible: false,
            selected: 'Välj',
            showService: false
        };
        this.clearText = this.clearText.bind(this);
    }

    static navigationOptions = {
        tabBarLabel: 'Boka Tid',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../../resources/icons/bookEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };

   _openModal() {
        this.setState({modalVisible: true});
   }

   _closeModal() {
      this.setState({modalVisible: false});
   }

    _handleCarModelSelect(selected) {
        this.setState({
            selected: (selected !=='Välj') ? selected : 'Välj'
        });

    }

    clearText() {
        this._textInput.setNativeProps({text: ''});
    }

    render(){
        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Kontaktuppgifter'}
                    headerBack={() => this.props.navigation.goBack()}
                />

                <ScrollView style={styles.scrollViewContainer}>
                    <View>
                        <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                            Fyll i dina Kontaktuppgifter nedan. Om du   
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row',}}>
                        <Text style={{ fontSize: 16, paddingBottom: 10, color: '#000',}}>
                            redan har ett konto - 
                        </Text>
                        <TouchableOpacity 
                            style={{paddingHorizontal: 5}}
                            onPress = {() => this.props.navigation.dispatch({ type: 'Login'})}>
                                <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                                    logga in här.
                                </Text> 
                        </TouchableOpacity>
                    </View>

                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Förenamn
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Förenamn'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />
      
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Efternamn
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Efternamn'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />
      
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Telefon
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Telefon'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />
      
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        E-post
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'E-post'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />
      

                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Kontakta mig via
                    </Text>              
                    <View style={styles.carModels}>
                        <TouchableOpacity  onPress = {() => this._openModal()}>
                            <Text style={[BASE_STYLES.TEXTFIELD.bodyText]}>
                                {this.state.selected}
                            </Text>
                        </TouchableOpacity>
                    </View>               
                    <ModelSelect 
                        modalVisible = {this.state.modalVisible} 
                        pickerValue = {'contactOptionPicker'}
                        // openModal = { () => this.openModal() }
                        selected = {this.state.selected}
                        selectCarModel = {(selected) => this._handleCarModelSelect(selected)}
                        closeModal = { () => this._closeModal() }/>   

                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Välj lösenord
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Lösenord'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Bekräfta lösenord'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        maxLength={20}
                        blurOnSubmit={false}e
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                    />

                    <TouchableOpacity onPress={() => { web('http://www.datainspektionen.se/lagar-och-regler/personuppgiftslagen/') }}>
                    <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                       PUL - Användarpolicy 
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style = {BASE_STYLES.BUTTON.bodyWide} 
                                onPress = {() => this.props.navigation.dispatch({ type: 'BookingOverview'})}>
                        <View style={{flex: 1}}>
                        </View>
                        <View style={styles.buttonTextWrap}>
                            <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                                Nästa
                            </Text>
                        </View>                    
                        <View style={styles.buttonSignWrap}>
                            <Text style={[BASE_STYLES.BUTTON.bodyWideButtonText, styles.buttonSign]}>
                            </Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        paddingHorizontal: 10,
        // marginHorizontal: 10,
    },
    icon: {
      width: 26,
      height: 26,
    },
    inputFieldPlaceHolder : {
        color: '#777777',
    },
    buttonTextWrap: {
        flex: 2,
        alignItems: 'center',
    },
    buttonSignWrap: {
        flex: 1,
        alignItems: 'flex-end',
    },
    buttonSign: {
        paddingRight: 10,
    },
    manualButtonImage: {
        flex: 1,
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },
    carModels: {
      height: 46,
      width: Dimensions.get('window').width,
      borderWidth: 1,
      borderRadius: 3,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 10, 
      marginBottom: 10,
      borderColor: '#F0F0F0',
      backgroundColor: '#eaeaec',
   }
});
