'use strict';

import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const BookingConfirmMessage = () => (
  <View style={styles.container}>
    <Text style={styles.welcome}>
      Your booking is confirmed.
    </Text>
  </View>
);

BookingConfirmMessage.navigationOptions = {
    title: 'Booking',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
    icon: {
    width: 26,
    height: 26,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});


export default BookingConfirmMessage;