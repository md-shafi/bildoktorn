'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput,  View, ScrollView, 
    TouchableHighlight, TouchableOpacity, Picker, Image, Dimensions } from 'react-native';

import Header from '../Header';
import { BASE_STYLES } from 'app/constants/baseStyles';
import ModelSelect from 'app/components/pickers/pickerOverlay';
import ServiceOptionSwitch from 'app/components/ServiceOptionSwitch';

export default class ServiceScreen extends Component {

    constructor(props) {
        super(props);
        this.state={
            showManualInput: false,
            modalVisible: false,
            selected: 'Välj',
            showService: false
        };
        this.clearText = this.clearText.bind(this);
    }

    static navigationOptions = {
        tabBarLabel: 'Boka Tid',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../../resources/icons/bookEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };


   _openModal() {
        this.setState({modalVisible: true});
   }

   _closeModal() {
      this.setState({modalVisible: false});
   }


    _handleCarModelSelect(selected) {
        this.setState({
            selected: (selected !=='Välj') ? selected : 'Välj'
        });

    }

    clearText() {
        this._textInput.setNativeProps({text: ''});
    }

    render(){
        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Service & tillval'}
                    headerBack={() => this.props.navigation.goBack()}
                />

            <ScrollView style={styles.scrollViewContainer}>
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Service
                </Text>
                <View style={styles.carModels}>
                    <TouchableOpacity  onPress = {() => this._openModal()}>
                        <Text style={[BASE_STYLES.TEXTFIELD.bodyText]}>
                            {this.state.selected}
                        </Text>
                    </TouchableOpacity>
                </View>               
                <ModelSelect 
                    modalVisible = {this.state.modalVisible} 
                    pickerValue= {'workshopPicker'}
                    // openModal = { () => this.openModal() }
                    selected = {this.state.selected}
                    selectCarModel = {(selected) => this._handleCarModelSelect(selected)}
                    closeModal = { () => this._closeModal() }/>        

                <View style={BASE_STYLES.SPACER}>
                </View> 
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Meddelande till verkstäden
                </Text>
                <TextInput 
                    style={[BASE_STYLES.INPUTFILED.basic, 
                        {justifyContent: 'flex-start', height: 92, fontSize: 16,}]}
                    placeholder={'Meddelande...'}
                    autoCorrect={false}
                    multiline={true}
                    textAlignVertical="top"
                    maxLength={200}
                    autoCapitalization='none'
                    blurOnSubmit={false}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    ref={component => this._textInput = component}
                    onPress={this.clearText}
                />
                <View style={BASE_STYLES.SPACER}>
                </View>                
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Tillval
                </Text> 
                <ServiceOptionSwitch />
                <TouchableOpacity style = {BASE_STYLES.BUTTON.bodyWide} 
                            onPress = {() => this.props.navigation.dispatch({ type: 'BookingTimeAndPlace'})}>
                    <View style={{flex: 1}}>
                    </View>
                    <View style={styles.buttonTextWrap}>
                        <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                            Nästa
                        </Text>
                    </View>                    
                    <View style={styles.buttonSignWrap}>
                        <Text style={[BASE_STYLES.BUTTON.bodyWideButtonText, styles.buttonSign]}>
                            
                        </Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        paddingHorizontal: 10,
    },
    icon: {
      width: 26,
      height: 26,
    },
    inputFieldPlaceHolder : {
        color: '#777777',
    },
    buttonTextWrap: {
        flex: 2,
        alignItems: 'center',
    },
    buttonSignWrap: {
        flex: 1,
        alignItems: 'flex-end',
    },
    buttonSign: {
        paddingRight: 10,
    },
    manualButtonImage: {
        flex: 1,
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },
    carModels: {
      height: 46,
      width: Dimensions.get('window').width,
      borderWidth: 1,
      borderRadius: 3,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 10, 
      marginBottom: 10,
      borderColor: '#F0F0F0',
      backgroundColor: '#eaeaec',
   }
});
