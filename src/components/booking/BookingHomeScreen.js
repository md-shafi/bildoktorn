'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput,  View, ScrollView, 
    TouchableHighlight, TouchableOpacity, Picker, Image, Dimensions } from 'react-native';

import { NavigationActions } from 'react-navigation';

import Header from '../Header';
import { BASE_STYLES } from 'app/constants/baseStyles';
import ModelSelect from 'app/components/pickers/pickerOverlay';

export default class Booking extends Component {

    constructor(props) {
        super(props);
        // data = ['Välj', 'AUDI', 'SEAT', 'SKODA', 'VOLKSWAGEN', 'VW TRP'];
        this.state={
            showManualInput: false,
            modalVisible: false,
            selected: 'Välj'
        };
        this.clearText = this.clearText.bind(this);
    }

    static navigationOptions = {
        title: 'Booking',
        tabBarLabel: 'Boka Tid',
        tabBarIcon: ({ tintColor }) => (
        <Image
            source={require('../../resources/icons/bookEnable.png')}
            style={[styles.icon, {tintColor: tintColor}]}
        />
        ),
    };


   _openModal() {
        this.setState({modalVisible: true});
   }

   _closeModal() {
      this.setState({modalVisible: false});
   }


    _handleManualInput() {
        this.setState({
            showManualInput: !this.state.showManualInput
        });
    }

    _handleCarModelSelect(selected) {
        this.setState({
            selected: (selected !=='Välj') ? selected : 'Välj'
        });

    }

    // _handleLogin() {
    //     this.props.showLogin();
    // }

    clearText() {
        this._textInput.setNativeProps({text: ''});
    }

    render(){
        let manualInfo = (
            <View>
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Märke
                </Text>
                <TouchableOpacity style={styles.carModels} onPress = {() => this._openModal()}>
                    <Text style={[BASE_STYLES.TEXTFIELD.bodyText]}>
                        {this.state.selected}
                    </Text>
                </TouchableOpacity>               
                <ModelSelect 
                    modalVisible = {this.state.modalVisible} 
                    pickerValue = {'vehiclePicker'}
                    // openModal = { () => this.openModal() }
                    selected = {this.state.selected}
                    selectCarModel = {(selected) => this._handleCarModelSelect(selected)}
                    closeModal = { () => this._closeModal() }/>        
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Modell
                </Text>                
                <TextInput 
                    style={BASE_STYLES.INPUTFILED.basic}
                    autoCorrect={false}
                    autoCapitalization='none'
                    blurOnSubmit={false}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    onSubmitEditing={(event) => { 
                        this.refs.modelYearInput.focus(); 
                    }}
                />
                <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                    Årsmodell
                </Text>                
                <TextInput 
                    ref='modelYearInput'
                    style={BASE_STYLES.INPUTFILED.basic}
                    placeholder={'Årsmodell'}
                    autoCorrect={false}
                    autoCapitalization='none'
                    blurOnSubmit={false}
                    maxLength={4}
                    underlineColorAndroid='rgba(0,0,0,0)' 
                    keyboardType = 'numeric'
                />
            </View>
            );

        return(
            <View style={styles.container}>
                <Header 
                    headerTitle={'Fordonsuppgifter'}
                />
                <ScrollView style={styles.scrollViewContainer}>
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Sök fordon i databasen
                    </Text>
                    <TextInput 
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Registeringsnummer'}
                        autoCorrect={false}
                        autoCapitalization='characters'
                        maxLength={20}
                        blurOnSubmit={false}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        ref={component => this._textInput = component}
                        onPress={this.clearText}
                        // onSubmitEditing={(event) => { this.refs.mileageInput.focus(); }}
                    />
                    <Text style={BASE_STYLES.TEXTFIELD.bodyText}>
                        Mätarställning i mil
                    </Text>                
                    <TextInput 
                        ref='mileageInput'
                        style={BASE_STYLES.INPUTFILED.basic}
                        placeholder={'Mil'}
                        autoCorrect={false}
                        autoCapitalization='none'
                        blurOnSubmit={false}
                        keyboardType = 'numeric'
                        maxLength={10}
                        underlineColorAndroid='rgba(0,0,0,0)' 
                    />
                    <View style={{paddingTop: 10}}>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableHighlight onPress={() => {this._handleManualInput()}} 
                             underlayColor='white' style={{flexDirection: 'row'}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={BASE_STYLES.TEXTFIELD.bodyLink}>
                                    Ange fordonuppgifter manulelt
                                </Text>
                                <View style={{paddingLeft: 10}}>
                                {this.state.showManualInput ? 
                                    <Image style={styles.manualButtonImage} source={require('app/resources/icons/Down32.png')}/> 
                                    : <Image style={styles.manualButtonImage} source={require('app/resources/icons/Right32.png')}/> 
                                }
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={{paddingBottom: 10}}>
                    </View>

                    {this.state.showManualInput ? manualInfo : null}

                    <TouchableOpacity style = {BASE_STYLES.BUTTON.bodyWide} 
                        onPress = {() => this.props.navigation.dispatch({ type: 'Service' })}
                    >
                        <View style={{flex: 1}}>
                        </View>
                        <View style={styles.buttonTextWrap}>
                            <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                                Nästa
                            </Text>
                        </View>                    
                        <View style={styles.buttonSignWrap}>
                            <Text style={[BASE_STYLES.BUTTON.bodyWideButtonText, styles.buttonSign]}>
                                
                            </Text>
                        </View>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        paddingHorizontal: 10,
    },
    icon: {
        width: 26,
        height: 26,
    },

    inputFieldPlaceHolder : {
        color: '#777777',
    },
    buttonTextWrap: {
        flex: 2,
        alignItems: 'center',
    },
    buttonSignWrap: {
        flex: 1,
        alignItems: 'flex-end',
    },
    buttonSign: {
        paddingRight: 10,
    },
    manualButtonImage: {
        flex: 1,
        height: 20,
        width: 20,
        resizeMode: 'contain',
    },
    carModels: {
      height: 46,
      width: Dimensions.get('window').width,
      borderWidth: 1,
      borderRadius: 3,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 10, 
      marginBottom: 10,
      borderColor: '#F0F0F0',
      backgroundColor: '#eaeaec',
   }
});



