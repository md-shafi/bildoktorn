'use strict';

import React, { Component } from 'react';
import { Switch, Text, TouchableHighlight, TouchableOpacity, View, StyleSheet, Image} from 'react-native';
import { connect } from 'react-redux';

const CAR_MODEL = ['AUDI', 'SEAT', 'SKODA', 'VOLKSWAGEN', 'VW TRP'];
import { COLORS } from 'app/constants/colors';
import { BASE_STYLES } from 'app/constants/baseStyles';

class CarFilter extends Component {
	constructor(){
		super();
		this.state = {
			audiSelected: true,
			seatSelected: true,
			skodaSelected: true,
			vwSelected: true,
			vwTrpSelected: true,
		};
	}

    // _getData(){
    //     let data = CAR_MODEL.map((data, index)=>{
    //         return(
    //         	<View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
    //         		<Text> {data}</Text>
    //         		        <Switch
    //       onValueChange={(value) => this.setState({colorFalseSwitchIsOn: value})}
    //       onTintColor="#00ff00"
    //       style={{marginBottom: 10}}
    //       thumbTintColor="#0000ff"
    //       tintColor="#ff0000"
    //       value={this.state.colorFalseSwitchIsOn} />
    //         	</View>
                
    //             );
    //     });
    //     return data;
    // }
// this.state.audiSelected ? COLORS.modalActiveBackground : seperatorBorderColor

	render() {
        return (
         	<View style={{ flex: 1}}>
                <View style={BASE_STYLES.MODAL.modalRow}>
                	<View>
                		<Text style={{fontSize: 18}}> AUDI </Text>
                	</View>
                	<View>
                		<Switch
    						onValueChange={(value) => this.props._filterAudi(value)}
    						onTintColor={COLORS.modalActiveBackground}
    						thumbTintColor={this.props.audiSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
    						tintColor={COLORS.iconMuteColor}
    						value={this.props.audiSelected} />
                	</View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                	<View>
                		<Text style={{fontSize: 18}}> SEAT </Text>
                	</View>
                	<View>
                		<Switch
    						onValueChange={(value) => this.props._filterSeat(value)}
    						onTintColor={COLORS.modalActiveBackground}
    						thumbTintColor={this.props.seatSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
    						tintColor={COLORS.iconMuteColor}
    						value={this.props.seatSelected} />
                	</View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                	<View>
                		<Text style={{fontSize: 18}}> SKODA </Text>
                	</View>
                	<View>
                		<Switch
    						onValueChange={(value) => this.props._filterSkoda(value)}
    						onTintColor={COLORS.modalActiveBackground}
    						thumbTintColor={this.props.skodaSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
    						tintColor={COLORS.iconMuteColor}
    						value={this.props.skodaSelected} />
                	</View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                	<View>
                		<Text style={{fontSize: 18}}> VOLKSWAGEN </Text>
                	</View>
                	<View>
                		<Switch
    						onValueChange={(value) => this.props._filterVw(value)}
    						onTintColor={COLORS.modalActiveBackground}
    						thumbTintColor={this.props.vwSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
    						tintColor={COLORS.iconMuteColor}
    						value={this.props.vwSelected} />
                	</View>
                </View>
                <View style={BASE_STYLES.MODAL.modalRow}>
                	<View>
                		<Text style={{fontSize: 18}}> VW TRP </Text>
                	</View>
                	<View>
                		<Switch
    						onValueChange={(value) => this.props._filterVwTrp(value)}
    						onTintColor={COLORS.modalActiveBackground}
    						thumbTintColor={this.props.vwTrpSelected ? COLORS.appThemeColor : COLORS.iconMuteColor}
    						tintColor={COLORS.iconMuteColor}
    						value={this.props.vwTrpSelected} />
                	</View>
                </View>
          	</View>
        );
  }
}

const mapStateToProps = (state) => {
    console.log('%%% audi' , state.carFilter.audiSelected);
    // console.log('%%% seat' , state.seatSelected);
    // console.log('%%% page:' , state.page);
    return{
        audiSelected: state.carFilter.audiSelected,
        seatSelected: state.carFilter.seatSelected,
        skodaSelected: state.carFilter.skodaSelected,
        vwSelected: state.carFilter.vwSelected,
        vwTrpSelected: state.carFilter.vwTrpSelected
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        _filterAudi: (value) => {
            dispatch({
                type: 'FILTER_AUDI',
                data: value
            });
        },
        _filterSeat: (value) => {
            dispatch({
                type: 'FILTER_SEAT',
                data: value
            });
        },
        _filterSkoda: (value) => {
            dispatch({
                type: 'FILTER_SKODA',
                data: value
            });
        },
        _filterVw: (value) => {
            dispatch({
                type: 'FILTER_VW',
                data: value
            });
        },
        _filterVwTrp: (value) => {
            dispatch({
                type: 'FILTER_VWTRP',
                data: value
            });
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarFilter);