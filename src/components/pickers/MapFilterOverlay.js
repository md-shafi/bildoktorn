'use strict';

import React, { Component } from 'react';
import { Modal, Switch, Text, TouchableHighlight, TouchableOpacity, View, StyleSheet, Image} from 'react-native';
import { connect } from 'react-redux';

import { BASE_STYLES } from 'app/constants/baseStyles';
import CarFilter from 'app/components/carFilter2';

// const CAR_MODEL = ['AUDI', 'SEAT', 'SKODA', 'VOLKSWAGEN', 'VW TRP'];

export default mapFilterModal = (props) => {
    return (
        <View style = {styles.container}>
            <Modal
                animationType = {"slide"}
                transparent = {true}
                visible = {props.modalVisible}
                onRequestClose = {() => {alert("Modal has been closed.")}}>
                <View style = {styles.modal}>
                   <View style={{flex: 1 }}>
                        <TouchableHighlight 
                            style={{position:'absolute', top: 10, right: 10}} 
                            onPress = {props.closeModal}>
                            <Image
                                source = {require('app/util/cancel32.png')}
                                resizeMode = 'contain'/>
                        </TouchableHighlight>
                    </View>
                    <View style = {{flex: 4, alignItems: 'center'}}>
                        <CarFilter/>
                    </View>
                    <View style = {{flex: 4}}>
                    </View>
                </View>
            </Modal>

            <TouchableOpacity style={BASE_STYLES.BUTTON.floating} onPress = {props.openModal}>
                <Text style={BASE_STYLES.TEXTFIELD.floatingButtonText}>
                    <View style={{ width: 20, height: 20, alignItems: 'center', }}>
                        <Image
                            source = {require('app/util/filter32.png')}
                            resizeMode = 'contain'/>
                    </View>
                </Text>
            </TouchableOpacity>

      </View>
   );
}

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 70
   },
   modal: {
      flex: 1,
      marginTop: 22,
      backgroundColor: 'white',
   }
});
