'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput,  View, ScrollView, TouchableOpacity, Picker, Image, Dimensions } from 'react-native';

import { BASE_STYLES } from 'app/constants/baseStyles';
// import ModelSelect from 'app/components/modelSelect';

export default class WorkshopPicker extends Component {

    constructor(props) {
        super(props);
        this.data = ['Välj', 'Haninge AUDI,SEAT', 'Nacka Volkswagon'];
        this.state={
            selected: null,
        };
    }

	_renderItem() { // array.map
        let items = this.data.map((item, index)=>{
            return( 
                <Picker.Item value={item} label={item} key={index} /> 
            );
        }); 
        return items;
    }

    render() {
    	return(
    		<View style={styles.container}>
                <Picker 
                    style={styles.picker}
                    selectedValue={this.props.selected} 
                    onValueChange={(selected) => {this.props.selectCarModel(selected)}}
                >
                    {this._renderItem()}
                </Picker>
    		</View>
    	);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    	justifyContent: 'flex-start',
    	// alignItems: 'center',
        // backgroundColor: 'black',
    },
    picker:{
    	width: Dimensions.get('window').width,
        // backgroundColor:'grey',
    },
})
