'use strict';

import React, { Component } from 'react';
import { Modal, Switch, Text, TouchableHighlight, TouchableOpacity, View, StyleSheet, Image, Dimensions} from 'react-native';

import { BASE_STYLES } from 'app/constants/baseStyles';
import VehicleBrandPicker from 'app/components/pickers/VehicleBrandPicker';
import WorkshopPicker from 'app/components/pickers/WorkshopPicker';
import ContactOptionPicker from 'app/components/pickers/ContactOptionPicker';


const {width, height} = Dimensions.get('window');

export default class modelSelect extends Component {

  _getPickerValue(pickerValue){
    switch (pickerValue) {
      case 'vehiclePicker':
        return(
            <VehicleBrandPicker 
              selected = {this.props.selected}
              selectCarModel = {this.props.selectCarModel}
            />
          );
        break;
      case 'workshopPicker':
        return(
            <WorkshopPicker 
              selected = {this.props.selected}
              selectCarModel = {this.props.selectCarModel}
            />
          );
        break;
      case 'contactOptionPicker':
        return(
            <ContactOptionPicker 
              selected = {this.props.selected}
              selectCarModel = {this.props.selectCarModel}
            />
          );
        break;
      default:
        return null;
    }
  }

   render() {
        // console.log("€€€", this.props.selected);
       return (
          <View style = {styles.container}>

             <Modal
                animationType = {"slide"}
                transparent = {true}
                visible = {this.props.modalVisible}
                onRequestClose = {() => {alert("Window closed")}}
                >
                <View style = {styles.modal}>
                   <View style={{flex: 4 }}>
                         <View style={{flex: 3 }}>
                      <TouchableHighlight 
                         style={{position:'absolute', top: 20, right: 20,}} 
                         onPress = {this.props.closeModal}>
                         <Image
                            source = {require('app/resources/icons/cancel32.png')}
                            resizeMode = 'contain'
                        />
                      </TouchableHighlight>
                        </View>
                      <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                      <Text style={BASE_STYLES.TEXTFIELD.headerText}>

                      </Text>
                      </View>
                   </View>
                   <View style = {{flex: 4}}>
                      {this._getPickerValue(this.props.pickerValue)}

                   </View>
                   <View style = {{flex: 4, paddingHorizontal: 30, justifyContent: 'flex-start'}}>
                        <TouchableOpacity style={BASE_STYLES.BUTTON.bodyWide} onPress = {this.props.closeModal}>
                            <Text style={BASE_STYLES.BUTTON.bodyWideButtonText}>
                                Välj
                            </Text>
                        </TouchableOpacity>
                   </View>
                </View>
             </Modal>
          </View>
       );
    }
    
}

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
   },
   modal: {
      flex: 1,
      marginTop: 22,
      backgroundColor: 'white',
      // backgroundColor: '#111',
      // opacity: 0.7,
   },
});