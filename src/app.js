import React, {Component} from 'react';
import { AppRegistry, StyleSheet, View, Text, StatusBar, Platform} from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import AppReducer from './reducers';
import AppWithNavigationState from './navigators/AppNavigator';
import { COLORS } from 'app/constants/colors';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 22 : 0;
const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar backgroundColor={backgroundColor} {...props} />
    </View>
);

class BilDoktornStockholm extends Component {
    store = createStore(AppReducer);
    render() {
        return (
            <Provider store={this.store}>
                <View style={styles.container}>
                <MyStatusBar backgroundColor={COLORS.appThemeColor} barStyle="light-content" />
                <AppWithNavigationState />
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});

AppRegistry.registerComponent('BilDoktornStockholm', () => BilDoktornStockholm);
