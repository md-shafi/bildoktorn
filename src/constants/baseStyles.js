'use-strict';

import { COLORS } from 'app/constants/colors';
import { SIZE } from 'app/constants/size';

export const BASE_STYLES = {
INPUTFILED: {
	basic: {
	    height: 46,
	    borderWidth: 1,
	    borderRadius: 3,
	    justifyContent: 'center',
	    paddingHorizontal: 10, 
	    marginBottom: 10,
	    borderColor: '#F0F0F0',
	    backgroundColor: '#eaeaec',
    },		
},
TEXTFIELD: {
    headerTitle: {
        fontWeight: '600',
        fontSize: 18,
        textAlign: 'center',
        color: COLORS.headerTitleColor,
    },
    headerText: {
        fontSize: 20,
        paddingVertical: 20,
        color: '#000'
    },
    bodyText: {
    	fontSize: 16,
        paddingVertical: 10,
        color: '#000',
    },
    bodyLink: {
        fontSize: SIZE.BodyLinkSize,
        // paddingTop: 20,
        // paddingBottom: 10,
        color: COLORS.bodyLinkColor,
    },
    floatingButtonText: {
		color: 'white',
		fontSize: 11,
	},
},

BUTTON: {
	navBar: {
	    width: 25,
	    height: 25
	},
	navBarButtonActive: {
		justifyContent: 'center',
	    alignItems: 'center',
	    paddingBottom: 1,
	},
	navBarButtonMute: {
		justifyContent: 'center',
	    alignItems: 'center',
	},
	floating: {
	    backgroundColor: '#3498db',
	    // backgroundColor: '#ff5722',
	    borderColor: '#3498db',
	    borderWidth: 1,
	    height: 50,
	    width: 50,
	    borderRadius: 25,
	    alignItems: 'center',
	    justifyContent: 'center',
	    position: 'absolute',
	    bottom: 20,
	    right:20,
	    shadowColor: "#000000",
	    shadowOpacity: 0.4,
	    shadowRadius: 5,
	    shadowOffset: {
	      	height: 5,
	      	width: 0
  		}
	},
	bodyWide: {
        flexDirection: 'row',
        marginTop: 25,
        height: 46,
        borderRadius: 3,
        // backgroundColor: '#337ab7',
        backgroundColor: COLORS.buttonBackgroundColor,
        // backgroundColor: '#0a60fe',
        alignItems: 'center',
        justifyContent: 'center',
	},
    bodyWideButtonText: {
        fontSize: 18,
        color: 'white',
    },
},

MODAL: { 
	modalRow: {
		flex: 1, 
		// width: 300, 
		paddingVertical: 7,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center', 
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderColor: COLORS.seperatorBorderColor,
		// backgroundColor: 'orange',
        justifyContent: 'space-between'
    }
},

SPACER: {
    alignSelf: 'flex-start',
    paddingTop: 5,
    paddingBottom: 20,
}

};