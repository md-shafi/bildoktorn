export const SIZE = {
	bodyTextSize: 16,
	headerTextSize: 20,
	headerTitleSize: 18,
	BodyLinkSize: 16,
	navBarTextSize: 11,
	buttonTextSize: 18,
}