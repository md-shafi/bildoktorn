export const initialState = {
	navigation: {  
        page: 'HOME',
        headerVisible: false,
        headerTitle: null,
        
        // homeVisible: true,
        // bookingVisible: false,
        // locationVisible: false,
        // profileVisible: false,
        // historyVisible: false,
        // loginVisible: false,
        // resetPassVisible: false,
        userLoggedIn: false,
        icon: 'homeEnable'
	},
    carFilter: {
        audiSelected: true,
        seatSelected: true,
        skodaSelected: true,
        vwSelected: true,
        vwTrpSelected: true
    }
};