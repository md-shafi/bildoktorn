'use strict';

import React from 'react';
import { BackHandler } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, NavigationActions, StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';


import { COLORS } from 'app/constants/colors';
import HomeScreen from 'app/components/home/HomeScreen';
import WorkshopScreen from 'app/components/WorkshopScreen';
import HistoryScreen from 'app/components/HistoryScreen';
import LoginScreen from 'app/components/LoginScreen';
import ResetPassScreen from 'app/components/ResetPassScreen';
import ProfileScreen from 'app/components/ProfileScreen';

// Booking Screens for StackNavigator
import BookingHomeScreen from '../components/booking/BookingHomeScreen';
import ServiceScreen from '../components/booking/ServiceScreen';
import BookingTimeAndPlaceScreen from '../components/booking/BookingTimeAndPlaceScreen';
import UserContactInfoScreen from '../components/booking/UserContactInfoScreen';
import BookingOverviewScreen from '../components/booking/BookingOverviewScreen';

const LoggedIn = false;

const BookingStack = StackNavigator(
	{
		BookingHome: { screen: BookingHomeScreen },
		Service: { screen: ServiceScreen },
		BookingTimeAndPlace: { screen: BookingTimeAndPlaceScreen },
		UserContactInfo: { screen: UserContactInfoScreen },
		BookingOverview: { screen: BookingOverviewScreen },
	},
	{
    	headerMode: 'none',
    	// headerLeft: null,
  	}
);

const LoginStack = StackNavigator(
	{
		LoginScreen: { screen: LoginScreen},
		ResetPass: { screen: ResetPassScreen },
	},
	{
    	headerMode: 'none',
    	// headerLeft: null,
  	}
);

export const AppNavigator = TabNavigator(
	{
		Home: { screen: HomeScreen },
		Booking: { screen: BookingStack },
		Workshop: { screen: WorkshopScreen },
		History: { screen: HistoryScreen },
		...(LoggedIn === true ? { Profile: { screen: ProfileScreen } } : 
								{ Login: { screen: LoginStack }}),
		// Profile: { screen: ProfileScreen },
	},
	{
		// tabBarComponent: NavigationComponent,
		tabBarComponent: TabBarBottom,
		tabBarPosition: 'bottom',
		tabBarOptions: {
			activeTintColor: COLORS.appThemeColor,
			labelStyle: {
				fontSize: 12,
			},
			style: {
				// backgroundColor: 'orange',
			},
			showIcon: true,
		},
		swipeEnabled: true,
	}
);

class AppWithNavigationState extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if (nav.index === 0) {
      return false;
    }
    dispatch(NavigationActions.back());
    return true;
  };
  render() {
    const { dispatch, nav } = this.props;
    return (
      <AppNavigator
        navigation={addNavigationHelpers({ dispatch, state: nav })}
      />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.navigation,
});

export default connect(mapStateToProps)(AppWithNavigationState);