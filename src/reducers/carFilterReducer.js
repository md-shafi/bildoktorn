'use strict';

import { initialState } from 'app/constants/initialState';

export default function carFilterReducer( state=initialState.carFilter, action ) {
    let newState;
    switch(action.type) {
        case 'FILTER_AUDI':
            newState = Object.assign({}, state, {
              audiSelected: action.data
            });
            return newState;
        case 'FILTER_SEAT':
            newState = Object.assign({}, state, {
              seatSelected: action.data
            });
            return newState;
        case 'FILTER_SKODA':
            newState = Object.assign({}, state, {
              skodaSelected: action.data
            });
            return newState;
        case 'FILTER_VW':
            newState = Object.assign({}, state, {
              vwSelected: action.data
            });
            return newState;
        case 'FILTER_VWTRP':
            newState = Object.assign({}, state, {
              vwTrpSelected: action.data
            });
            return newState;
        default:
            return state; 
    }
}
