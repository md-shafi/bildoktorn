'use strict';

import { combineReducers } from 'redux';
import navigation from 'app/reducers/navigationReducer';
import carFilter from 'app/reducers/carFilterReducer';

const AppReducer = combineReducers({
  navigation,
  carFilter
});

export default AppReducer;