'use strict';


import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigators/AppNavigator';

// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = AppNavigator.router.getActionForPathAndParams('Home');

const initialNavState = AppNavigator.router.getStateForAction(
  firstAction
);

export default function navigationReducer(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case 'Login':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Login' }),
        state
      );
      break;
    case 'Service':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Service' }),
        state
      );
      break;
    case 'Booking':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Booking' }),
        state
      );
      break;
    case 'BookingTimeAndPlace':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'BookingTimeAndPlace' }),
        state
      );
      break;
    case 'UserContactInfo':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'UserContactInfo' }),
        state
      );
      break;
    case 'BookingOverview':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'BookingOverview' }),
        state
      );
      break;
    case 'ResetPass':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ResetPass' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}